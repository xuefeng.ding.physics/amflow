(* ::Package:: *)

(*path to FiniteFlow.m*)
$FFPath = "/mnt/zfsusers/xiao6/research/mmapkg/external/finiteflow";
(*path to fflowmlink.so*)
$FFLibraryPath = "/mnt/zfsusers/xiao6/research/local/ffmath";
(*path to LiteIBP.m*)
$LiteIBPPath = "/mnt/zfsusers/xiao6/research/mmapkg/external/finiteflow-mathtools-master/packages";
(*path to LiteRed.m*)
$LiteRedPath = "/mnt/zfsusers/xiao6/research/mmapkg/external/LiteRed";
