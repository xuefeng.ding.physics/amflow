AMFlow
======

This is a proof-of-concept implementation of the auxiliary mass flow method. 

Auxiliary mass flow is a method for numerically evaluating Feynman integrals and phase-space integrations with arbitrary precision in dimensional regularization scheme. By introducing an auxiliary mass parameter to the original integral family, all integrals in this family become combinations of much simpler integrals when the auxiliary parameter approaches the infinity. Then original Feynman integrals are obtained through the analytical continuation from the infinity to 0, which can be realized using numerical differential equation techniques.

It is based on the following publications:
- Xiao Liu, Yan-Qing Ma and Chen-Yu Wang, A Systematic and Efficient Method to Compute Multi-loop Master Integrals, [arXiv:1711.09572]
- Xiao Liu, Yan-Qing Ma, Wei Tao and Peng Zhang, Calculation of Feynman loop integration and phase-space integration via auxiliary mass flow, [arXiv:2009.07987]
- Xiao Liu and Yan-Qing Ma, Multiloop corrections for collider processes using auxiliary mass flow, [arXiv:2107.01864]
- Xiao Liu and Yan-Qing Ma, [arXiv:2201.11669]

As a by product, this tool also provides a solver [diffeq_solver/DESolver.m] for ordinary differential equations.


Dependencies
============

In general, this tool depends on external programs to do integral reductions. Currently only interfaces to FiniteFlow+LiteRed and Kira are provided. But interface to any other program can be added freely by users according to their own requirements. (If you just want to use the differential equations solver in this tool, then no external package is needed.)

#### FiniteFlow+LiteRed

If you prefer to use FiniteFlow+LiteRed as a reducer, you should first install the following packages:
- FiniteFlow (https://github.com/peraro/finiteflow/);
- LiteIBP.m (https://github.com/peraro/finiteflow-mathtools/);
- LiteRed v1.83 or later (https://www.inp.nsk.su/~lee/programs/LiteRed/).

Then reset variables in ibp_interface/FiniteFlow/install.m:
- $FFPath, as path to [FiniteFlow.m], e.g., "/home/abc/finiteflow-master/mathlink";
- $FFLibraryPath, as path to [fflowmlink.so], e.g., "/home/abc/finiteflow-master";
- $LiteIBPPath, as path to [LiteIBP.m], e.g., "/home/abc/finiteflow-mathtools-master/packages";
- $LiteRedPath, as path to [LiteRed.m], e.g., "/home/abc/LiteRed".

#### Kira

If you prefer to use Kira as a reducer, you should first install:
- Kira v2.2 or later (https://kira.hepforge.org/);
- Fermat (http://home.bway.net/lewis/).

Then reset variables in ibp_interface/Kira/install.m:
- $KiraExecutable, as path to Kira executable, e.g., "/home/abc/kira-2.2";
- $FermatExecutable, as path to Fermat executable, e.g., "/home/abc/ferl6/fer64".


Usage
=====

Several examples are provided to explain the basic usage of this tool:
- examples/box - introduction to automatic evaluations of Feynman loop integrals;
- examples/higgs_RR - introduction to automatic evaluations of phase-space integrations using reverse unitarity;
- examples/tt - introduction to automatic evaluations and manual evaluations in case that automatic evaluations cannot give satisfactory results, including options setting, computing with given epsilon values and fitting;
- examples/sunrise - introduction to usage of differential equations solver, including validations of the results and computations of asymptotic expansions;
- examples/gaugelink - introduction to automatic evaluations of integrals with gauge link.

A folder named "backup" is attached in each example, which contains some outputs during the evaluations serving as benchmarks.

NOTE: the prefactor of each loop integral is (1/(i Pi^(2-eps)))^L, with L the loop number.


Contact
=======

If you have any questions or advices, please do not hesitate to contact us: xiao.liu@physics.ox.ac.uk, yqma@pku.edu.cn.