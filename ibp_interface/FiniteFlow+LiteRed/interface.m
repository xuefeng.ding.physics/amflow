(* ::Package:: *)

(* ::Subsubsection::Closed:: *)
(*begin*)


BeginPackage["FiniteFlow`", {"AMFlow`"}];


$FFPath::usage = "path to FiniteFlow.m.";
$FFLibraryPath::usage = "path to fflowmlink.so.";
$LiteIBPPath::usage = "path to LiteIBP.m.";
$LiteRedPath::usage = "path to LiteRed.m.";


Begin["`Private`"];


$WolframPath = FileNameJoin[{$InstallationDirectory, "Executables", "wolfram"}];
$Current = DirectoryName[$InputFileName];
$CTX = $Context;


Family := AMFlowInfo["Family"];
Loop := AMFlowInfo["Loop"];
IndepLeg := Select[AMFlowInfo["Leg"], !MemberQ[Keys[AMFlowInfo["Conservation"]], #]&];
SPToSTU:= Module[{complete,keys,values,bare,full},
If[IndepLeg === {}, Return[{}]];
complete = Outer[Times, IndepLeg, IndepLeg]//Flatten//DeleteDuplicates;
keys = Expand[Keys[AMFlowInfo["Replacement"]]/.AMFlowInfo["Conservation"]];
values = Values[AMFlowInfo["Replacement"]];
bare = Coefficient[#, complete]&/@keys;
full = Append[Transpose[bare],values]//Transpose;
If[MatrixRank[bare]<Length[complete], Print["SPToSTU: insufficient replacement rules for all independent external scalar products."]; Abort[]];
If[MatrixRank[full]>Length[complete], Print["SPToSTU: inconsistent replacement rules."]; Abort[]];
Thread[complete -> RowReduce[full][[;;Length[complete], -1]]]
];
Propagator := AMFlowInfo["Propagator"]/.AMFlowInfo["Conservation"];
Cut := AMFlowInfo["Cut"];
IBPRule := AMFlowInfo["Numeric"];
NThread := AMFlowInfo["NThread"];


$AuxLeg:=Symbol[ToStringInput[Family]<>"AuxLeg"];
Leg:=Append[IndepLeg, $AuxLeg];
Conservation:={$AuxLeg -> -Total[IndepLeg]};


STUToSP:=Block[{right, var, col, mat, group},
right = Values[SPToSTU];
var = Variables[right];
If[var==={}, Return[{}]];
{col, mat} = CoefficientArrays[right, var]//Normal;
group = MaximalGroup[mat];
Solve[Thread[Keys[SPToSTU]==Values[SPToSTU]][[group]], var][[1]]//Expand];


Momentum:=ToSquareAll[Propagator][[1]];
Mass:=ToSquareAll[Propagator][[2]];


MassScale:=Join[Variables[Values[SPToSTU]], Complement[Variables[Propagator], Loop, IndepLeg]]//DeleteDuplicates;


(* ::Subsubsection::Closed:: *)
(*topology*)


TopologyTemplate[dir_]:=Module[{template,rule},
template = StringToTemplate[{
"SetDim[4-2eps];
Internal = `loop`;
External = `leg`;
MomentumConservation = `momcon`;
Replacements = `rep`;
Invariants = `inv`;
LoopMomenta = `mom`;
Mass = `mass`;
Declare[#,Number]&/@`number`;
Propagators = ((mp2[#])&/@LoopMomenta+Mass)/. MomentumConservation/.Replacements//Expand;
LIBPFamilyLite[`fam`, Propagators, Internal, External, MomentumConservation, Replacements, Invariants];
NewBasis[`fam`,LIBPToLiteRed[Propagators],LIBPLoopMomenta[`fam`],GenerateIBP->True];
AnalyzeSectors[`fam`,`topsec`,CutDs->`cut`];
LIBPFindSymmetries[`fam`,EMs->True];"
}];

rule = Module[{mm,mmp,mmp2,sprule,momrule,momcon,rep,inv,mom,mass,number,toprule},
mm = Symbol["mm"];
mmp = Symbol["mmp"];
mmp2 = Symbol["mmp2"];
sprule = Join@@Table[If[i===j,Leg[[i]]Leg[[j]]->mmp2[Leg[[i]]],Leg[[i]]Leg[[j]]->mmp[Leg[[i]],Leg[[j]]]],{i,Length@Leg},{j,i,Length@Leg}];
momrule = Join[Loop,Leg]->mm/@Join[Loop,Leg]//Thread;

momcon = Conservation/.momrule;
rep = Thread[(SPToSTU[[All,1]]/.sprule)->SPToSTU[[All,2]]];
inv = Thread[STUToSP[[All,1]]->(Expand@STUToSP[[All,2]]/.sprule)];
mom = Expand@Momentum/.momrule;
mass = Mass;
number = MassScale;

<|
"loop" -> ToStringInput[Loop],
"leg" -> ToStringInput[Leg],
"momcon" -> ToStringInput[momcon],
"rep" -> ToStringInput[rep],
"inv" -> ToStringInput[inv],
"mom" -> ToStringInput[mom],
"mass" -> ToStringInput[mass],
"number" -> ToStringInput[number],
"fam" -> ToStringInput[Family],
"topsec" -> ToStringInput[TopSector],
"cut" -> ToStringInput[Cut]
|>
];

FileTemplateApply[template, rule, FileNameJoin[{dir, "topology.wl"}]];
];


(* ::Subsubsection::Closed:: *)
(*ibp*)


IBPParameter:=Select[Prepend[MassScale, Symbol["eps"]], !MemberQ[Keys[IBPRule],#]&];


IBPTemplate[preferred_, dir_]:=Module[{template,exints,rule},
template = StringToTemplate[{
"AppendTo[$Path,\"`litered`\"];
AppendTo[$Path,\"`liteibp`\"];
AppendTo[$Path,\"`ff`\"];
AppendTo[$Path,\"`sup`\"];
AppendTo[$LibraryPath,\"`fflib`\"];
If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName//SetDirectory;
<<LiteIBP.m;
<<sup.m;
<<topology.wl;",

"If[NonZeroSectors[`fam`]==={}, Print[\"this family is trivial.\"]; Put[Null, \"trivial\"]; Quit[]];",

"rank=`ibprank`;
maxdots=`ibpdot`;
GetSeeds[\"IBP\"][sector_]:=LIBPGenerateSeeds[sector,{0,maxdots},0,rank];
GetSeeds[\"LI\"][sector_]:=LIBPGenerateSeeds[sector,{0,maxdots},0,rank];
GetSeeds[\"SR\"][sector_]:=LIBPGenerateSeeds[sector,{0,0},0,rank];
GetSeeds[\"Map\"][sector_]:=LIBPGenerateSeeds[sector,{0,maxdots+1},0,rank];
GetSeeds[\"ExtMap\"][sector_]:=LIBPGenerateSeeds[sector,{0,maxdots+1},0,rank];
LIBPFastGenIds[`fam`,GetSeeds,\"Directory\"->\"ibps\",\"LaunchKernels\"->`NThread`];",

"exints = `exints`;
Export[\"ibps/exints_def.m\",exints];
Export[\"ibps/ids_`fam`_exints.mx\",(-#[[1]]+#[[2]])&/@exints,\"MX\"];
Export[\"ibps/ints_`fam`_exints.mx\",LIBPIntegralsIn[exints],\"MX\"];",

"LIBPSerializeFastIds[FileNames[\"ibps/ids_`fam`_*.mx\"], `ibprule`, `IBPParameter`, \"ExtraInts\"->(First/@exints)];",

"Quit[];"
}];

exints = Table[Symbol["ex"][i] -> preferred[[i]],{i,Length@preferred}];

rule = <|
"litered" -> $LiteRedPath,
"liteibp" -> $LiteIBPPath,
"ff" -> $FFPath,
"fflib" -> $FFLibraryPath,
"sup" -> $Current,
"ibprank" -> ToStringInput[IBPRank],
"ibpdot" -> ToStringInput[IBPDot],
"fam" -> ToStringInput[Family],
"NThread" -> ToStringInput[NThread],
"exints" -> ToStringInput[exints],
"ibprule" -> ToStringInput[IBPRule],
"IBPParameter" -> ToStringInput[IBPParameter]
|>;

FileTemplateApply[template, rule, FileNameJoin[{dir, "ibp.wl"}]];
];


(* ::Subsubsection::Closed:: *)
(*reduction-basic*)


BasicCode[]:={
"AppendTo[$Path,\"`litered`\"];
AppendTo[$Path,\"`liteibp`\"];
AppendTo[$Path,\"`ff`\"];
AppendTo[$Path,\"`sup`\"];
AppendTo[$LibraryPath,\"`fflib`\"];
If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName//SetDirectory;
If[FileExistsQ[\"trivial\"], Put[{{}, {}}, \"results/table\"]; Abort[]];
If[!DirectoryQ[\"ibps\"], Abort[]];
<<LiteIBP.m;
<<sup.m;
<<topology.wl;",

"all = LIBPGetAllInts[FileNames[\"ibps/ints_`fam`_*.mx\"]];
target = Get[\"target\"];
If[target === 0,
target = Select[all, LIBPIntS[#]<=`s` && ((LIBPIntR[#]-LIBPIntT[#])<=`d`)&],
target = Select[LIBPEliminateZeroSectors[target], #=!=0&]];
If[target === {}, Print[\"target is empty.\"]; Put[{{},{}}, \"results/table\"]; Abort[]];",

"exints = Import[\"ibps/exints_def.m\"];
all = Join[all, First/@exints];
eqsfiles = FileNames[\"ibps/sids_*.json\"];
vars = `IBPParameter`;
LIBPWriteSystemJSON[eqsfiles, all, target, vars, \"FileName\"->\"system.json\"];",

"FFNewGraph[graph,in,vars];
FFAlgJSONSparseSolver[graph,ibps,{in},\"system.json\"];
FFSolverOnlyHomogeneous[graph,ibps];
FFGraphOutput[graph,ibps];
ibplearn = FFSparseSolverLearn[graph,all];
{nonmis, mis} = {\"DepVars\", \"IndepVars\"}/.ibplearn;
Print[\"number of master integrals\" -> Length[mis]];
mastersexpr = mis/.exints;"
};


QuitCode[]:={
"FFDeleteGraph[graph];
Quit[];"
};


(* ::Subsubsection::Closed:: *)
(*reduction-analytic*)


AnalyticReductionTemplate[dir_]:=Module[{extra,template,rule},
extra = {
"If[mis === {}, Put[{{},{}}, \"results/table\"]; Abort[]];",

"FFSparseSolverMarkAndSweepEqs[graph,ibps];
FFSparseSolverDeleteUnneededEqs[graph,ibps];
Print[\"number of independent eqs.\"->FFSolverNIndepEqs[graph,ibps]];
sol = FFReconstructFunction[graph,vars,\"MaxPrimes\"->`maxp`,\"MaxDegree\"->`maxd`];
sol = Partition[sol, Length@mis]//Factor;
sol = Join[sol, IdentityMatrix[Length@mis]];
rule = Thread[Join[nonmis, mastersexpr]->sol];
Put[{mastersexpr, rule}, \"results/table\"];"
};
template = StringToTemplate[Join[BasicCode[], extra, QuitCode[]]];
rule = <|
"litered" -> $LiteRedPath,
"liteibp" -> $LiteIBPPath,
"ff" -> $FFPath,
"fflib" -> $FFLibraryPath,
"sup" -> $Current,
"fam" -> ToStringInput[Family],
"s" -> ToStringInput[IBPRank],
"d" -> ToStringInput[IBPDot],
"IBPParameter" -> ToStringInput[IBPParameter],
"maxp" -> ToStringInput[$MaxPrime],
"maxd" -> ToStringInput[$MaxDegree]
|>;

FileTemplateApply[template, rule, FileNameJoin[{dir, "reductionAna.wl"}]];
];


(* ::Subsubsection::Closed:: *)
(*reduction-diffeq*)


CheckCode[]:={
"checkedmis = Select[mis, Head[#] === ex || (LIBPIntR[#] === LIBPIntT[#] && LIBPIntS[#] <= `s2`) &];
fakemis = Complement[mis, checkedmis];
Print[\"these integrals are thought to be nonmis\" -> fakemis];
Print[\"use checked mis instead of mis.\"];
mis = checkedmis;
mastersexpr = mis/.exints;",

"max[int_]:=LIBPIntegralsIn[int][[1]];
maxlist = max/@mastersexpr;
sortmaxlist = Reverse[Join@@GatherBy[SortBy[maxlist,LIBPDefaultWeight],LIBPSectorOf]];
perm = Position[maxlist,#]&/@sortmaxlist//Flatten;
mastersexpr = mastersexpr[[perm]];",

"invariants = `inv`;
LIBPComputeDerivatives[`fam`];
computederivative[x_]:=LIBPEliminateZeroSectors[Collect[LIBPDeriv[#,x]&/@mastersexpr,_j,Together]];
derivatives = computederivative/@invariants/.`ibprule`;
new = LIBPIntegralsIn[derivatives];
new = Join[new,mis];",

"If[!SubsetQ[Join[nonmis,mis],new],Print[\"error: current ibp system is not sufficient to reduce all derivatives.\"];Abort[]];
FFSolverResetNeededVars[graph,ibps,all,new];
newibplearn = FFSparseSolverLearn[graph,all];
If[mis =!= (\"IndepVars\"/.newibplearn), Print[\"error: derivatives are reduced to a different set of master integrals.\"];Abort[]];
{nonmis, mis} = {\"DepVars\", \"IndepVars\"} /. newibplearn;
Put[{mastersexpr, invariants, derivatives}, \"results/diffeqinfo\"];
If[Length[mis]>`critical`, Print[\"too many masters.\"];Abort[]];"
};


DifferentialEquationTemplate[inv_,dir_]:=Module[{extra,template,rule},
extra = {
"FFSparseSolverMarkAndSweepEqs[graph,ibps];
FFSparseSolverDeleteUnneededEqs[graph,ibps];
Print[\"number of independent eqs.\"->FFSolverNIndepEqs[graph,ibps]];",

"ibpintegrals = Join[nonmis,mis];
FFAlgRatNumEval[graph,misred,Join@@IdentityMatrix[Length[mis]]];
FFAlgChain[graph,ibpsfull,{ibps,misred}];
FFAlgRatFunEval[graph, unreduced, {in},vars,Join@@(FFLinearCoefficients[#,ibpintegrals]&/@(Join@@derivatives))];
FFAlgMatMul[graph,de,{unreduced,ibpsfull},Length[invariants]*Length[mis], Length[ibpintegrals], Length[mis]];
FFAlgNonZeroes[graph,denz,{de}];
FFGraphOutput[graph,denz];
nzlearn = FFNonZeroesLearn[graph];
rec0 = FFReconstructFunction[graph,vars,\"MaxPrimes\"->`maxp`,\"MaxDegree\"->`maxd`];
rec = Normal[FFNonZeroesSol[rec0,nzlearn]];
deall = Factor@ArrayReshape[rec,{Length[invariants],Length[mis],Length[mis]}];",

"Table[deall[[i]] = deall[[i,All,perm]],{i,Length@deall}];
Put[{mastersexpr,invariants,deall},\"results/diffeq\"];"
};
template = StringToTemplate[Join[BasicCode[], CheckCode[], extra, QuitCode[]]];
rule = <|
"litered" -> $LiteRedPath,
"liteibp" -> $LiteIBPPath,
"ff" -> $FFPath,
"fflib" -> $FFLibraryPath,
"sup" -> $Current,
"fam" -> ToStringInput[Family],
"s" -> ToStringInput[IBPRank],
"d" -> ToStringInput[IBPDot],
"IBPParameter" -> ToStringInput[IBPParameter],
"s2" -> ToStringInput[Max[0, IBPRank - $ExcludeLevel]],
"inv" -> ToStringInput[inv],
"ibprule" -> ToStringInput[IBPRule],
"critical" -> ToStringInput[$CriticalMasters],
"maxp" -> ToStringInput[$MaxPrime],
"maxd" -> ToStringInput[$MaxDegree]
|>;

FileTemplateApply[template, rule, FileNameJoin[{dir, "reductionDe.wl"}]];
];


(* ::Subsubsection::Closed:: *)
(*usage*)


CheckDep[]:=Module[{},
Print["CheckDep: dependencies of current reducer:"];
Print["FiniteFlow.m" -> FileExistsQ[FileNameJoin[{$FFPath, "FiniteFlow.m"}]]];
Print["fflowmlink.so" -> FileExistsQ[FileNameJoin[{$FFLibraryPath, "fflowmlink.so"}]]];
Print["LiteIBP.m" -> FileExistsQ[FileNameJoin[{$LiteIBPPath, "LiteIBP.m"}]]];
Print["LiteRed.m" -> FileExistsQ[FileNameJoin[{$LiteRedPath, "LiteRed.m"}]]];
];


Options[SetReducerOptions] = {"ExcludeLevel" -> 0, "CriticalMasters" -> 1000, "MaxPrime" -> 200, "MaxDegree" -> 1000};
SetReducerOptions[opt___]:=Block[{},
If[MemberQ[Keys[{opt}], "ExcludeLevel"], $ExcludeLevel = "ExcludeLevel"/.{opt}];
If[MemberQ[Keys[{opt}], "CriticalMasters"], $CriticalMasters = "CriticalMasters"/.{opt}];
If[MemberQ[Keys[{opt}], "MaxPrime"], $MaxPrime = "MaxPrime"/.{opt}];
If[MemberQ[Keys[{opt}], "MaxDegree"], $MaxDegree = "MaxDegree"/.{opt}];

PrintOptions[SetReducerOptions, $CTX];
];


IBPSystem[top_,rank_,dot_,preferred_,dir_]:=Module[{time},
CheckCompleteness[];
$ReductionDirectory = dir;
DeleteDir[dir];
CreateDir[dir];
TopSector = top;
IBPRank = rank;
IBPDot = dot;
TopologyTemplate[dir];
IBPTemplate[preferred, dir];
Print[StringTemplate["IBPSystem: generating ibp system with rank `1` and dot `2`."][ToStringInput[IBPRank], ToStringInput[IBPDot]]];
time = RunCommand[{$WolframPath, "-noprompt", "-script", FileNameJoin[{dir, "ibp.wl"}]}, "log" -> FileNameJoin[{dir, "ibp.log"}]];
Print[StringTemplate["IBPSystem: ibp system generated in `1`s."][ToStringInput[Ceiling[time]]]];
];


AnalyticReduction[target_]:=Module[{dir, time, res},
dir = $ReductionDirectory;
AnalyticReductionTemplate[dir];
Put[target, FileNameJoin[{dir, "target"}]];
CreateDir[FileNameJoin[{dir, "results"}]];
Print[StringTemplate["AnalyticReduction: reducing `1` target integrals."][ToStringInput[Length[target]]]];
time = RunCommand[{$WolframPath, "-noprompt", "-script", FileNameJoin[{dir, "reductionAna.wl"}]}, "log" -> FileNameJoin[{dir, "reductionAna.log"}]];
res = GetFile[FileNameJoin[{$ReductionDirectory, "results/table"}]];
Print[StringTemplate["AnalyticReduction: target integrals reduced to `1` master integrals in `2`s."][ToStringInput[Length[res[[1]]]], ToStringInput[Ceiling[time]]]];
res
];


DifferentialEquation[inv_]:=Module[{dir, time, res},
dir = $ReductionDirectory;
DifferentialEquationTemplate[inv, dir];
Put[0, FileNameJoin[{dir, "target"}]];
CreateDir[FileNameJoin[{dir, "results"}]];
Print[StringTemplate["DifferentialEquation: constructing differential equations with respect to `1`."][ToStringInput[inv]]];
time = RunCommand[{$WolframPath, "-noprompt", "-script", FileNameJoin[{dir, "reductionDe.wl"}]}, "log" -> FileNameJoin[{dir, "reductionDe.log"}]];
res = GetFile[FileNameJoin[{$ReductionDirectory, "results/diffeq"}]];
Print[StringTemplate["DifferentialEquation: differential equations constructed with `1` master integrals in `2`s."][ToStringInput[Length[res[[1]]]], ToStringInput[Ceiling[time]]]];
res
];


(* ::Subsubsection::Closed:: *)
(*end*)


GetFile[FileNameJoin[{$Current, "install.m"}]];
CheckDep[];
SetReducerOptions@@Options[SetReducerOptions];


End[];


EndPackage[];
