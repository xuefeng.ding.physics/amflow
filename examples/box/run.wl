(* ::Package:: *)

(*load the package*)
current = If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName;
Get[FileNameJoin[{current, "..", "..", "AMFlow.m"}]];


(*set ibp reducer, could be "FiniteFlow+LiteRed" or "Kira"*)
SetReductionOptions["IBPReducer" -> "Kira"];


(*configuration of box1*)
AMFlowInfo["Family"] = box1;
AMFlowInfo["Loop"] = {l};
AMFlowInfo["Leg"] = {p1, p2, p3, p4};
AMFlowInfo["Conservation"] = {p4 -> -p1-p2-p3};
AMFlowInfo["Replacement"] = {p1^2 -> 0, p2^2 -> 0, p3^2 -> 0, p4^2 -> 0, (p1+p2)^2 -> s, (p1+p3)^2 -> t};
AMFlowInfo["Propagator"] = {l^2, (l+p1)^2, (l+p1+p2)^2, (l+p1+p2+p4)^2};
AMFlowInfo["Cut"] = {0, 0, 0, 0};
AMFlowInfo["Numeric"] = {s -> 100, t -> -1};
AMFlowInfo["NThread"] = 4;


(*evaluate given integrals with given precision goal up to given eps order*)
(*values of masters are also included*)
integrals = {j[box1,2,0,1,0], j[box1,-2,1,1,2], j[box1,1,2,2,1]};
precision = 40;
order = 2;
sol1 = SolveIntegrals[integrals, precision, order];
Put[sol1, FileNameJoin[{current, "sol1"}]];


(*configuration of box2*)
AMFlowInfo["Family"] = box2;
AMFlowInfo["Loop"] = {l};
AMFlowInfo["Leg"] = {p1, p2, p3, p4};
AMFlowInfo["Conservation"] = {p4 -> -p1-p2-p3};
AMFlowInfo["Replacement"] = {p1^2 -> 0, p2^2 -> 0, p3^2 -> 0, p4^2 -> 0, (p1+p2)^2 -> s, (p1+p3)^2 -> t};
AMFlowInfo["Propagator"] = {l^2, (l+p1)^2, (l+p1+p2)^2, (l+p1+p2+p3)^2};
AMFlowInfo["Cut"] = {0, 0, 0, 0};
AMFlowInfo["Numeric"] = {s -> 100, t -> -99};
AMFlowInfo["NThread"] = 4;


sol2 = SolveIntegrals[{j[box2, 2,1,1,1],j[box2, 1,1,0,1],j[box2, 1,-1,1,0]}, 20, 3];
Put[sol2, FileNameJoin[{current, "sol2"}]];


Quit[];
