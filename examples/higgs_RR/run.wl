(* ::Package:: *)

(*load the package*)
current = If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName;
Get[FileNameJoin[{current, "..", "..", "AMFlow.m"}]];


(*set ibp reducer, could be "FiniteFlow+LiteRed" or "Kira"*)
SetReductionOptions["IBPReducer" -> "Kira"];


(*set reduction mode for kira, could be "Kira", "FireFly", "Mixed" and "NoFactorScan", "Kira" by default*)
(*each reducer has its own options, defined in reducer/interface.m*)
SetReducerOptions["ReductionMode" -> "FireFly"];


(*configuration of the integral family*)
AMFlowInfo["Family"] = HiggsRR;
AMFlowInfo["Loop"] = {l1, l2};
AMFlowInfo["Leg"] = {p1, p2};
AMFlowInfo["Conservation"] = {};
AMFlowInfo["Replacement"] = {p1^2 -> 0, p2^2 -> 0, (p1+p2)^2 -> s};
AMFlowInfo["Propagator"] = {l1^2-msq, (l1+p1)^2, l2^2, (l1+l2+p1)^2, (l1+l2+p1+p2)^2, (l1+l2+p2)^2, (l1+p2)^2};
AMFlowInfo["Cut"] = {1, 0, 1, 0, 1, 0, 0};
AMFlowInfo["Numeric"] = {s -> 100, msq -> 1};
AMFlowInfo["NThread"] = 4;


(*evaluate given integrals with given precision goal up to given eps order*)
(*values of masters are also included*)
integrals = {j[HiggsRR,1,2,1,1,1,1,1]};
precision = 20;
order = 4;
sol = SolveIntegrals[integrals, precision, order];
Put[sol, FileNameJoin[{current, "sol"}]];


Quit[];
