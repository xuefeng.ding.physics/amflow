(* ::Package:: *)

(*load the package*)
current = If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName;
Get[FileNameJoin[{current, "..", "..", "AMFlow.m"}]];


(*set ibp reducer, could be "FiniteFlow+LiteRed" or "Kira"*)
SetReductionOptions["IBPReducer" -> "Kira"];


(*configuration of the integral family*)
AMFlowInfo["Family"] = tt;
AMFlowInfo["Loop"] = {l1, l2};
AMFlowInfo["Leg"] = {p1, p2, p3, p4};
AMFlowInfo["Conservation"] = {p4 -> -p1-p2-p3};
AMFlowInfo["Replacement"] = {p1^2 -> 0, p2^2 -> 0, p3^2 -> msq, p4^2 -> msq, (p1+p2)^2 -> s, (p1+p3)^2 -> t};
AMFlowInfo["Propagator"] = {l1^2, (l1+p1)^2, (l1+p1+p2)^2, l2^2, -msq+(l2+p3)^2, (l2+p3+p4)^2, (l1+l2)^2, (l1-p3)^2, (l2+p1)^2};
AMFlowInfo["Cut"] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
AMFlowInfo["Numeric"] = {s -> 30, t -> -10/3, msq -> 1};
AMFlowInfo["NThread"] = 4;


(*automatic evaluation of target integrals with given precision goal up to given eps order*)
(*results of master integrals are also included*)
target = {j[tt,1,1,1,1,1,1,1,-3,0], j[tt,1,1,1,1,1,1,1,-2,-1], j[tt,1,1,1,1,1,1,1,-1,-2], j[tt,1,1,1,1,1,1,1,0,-3]};
precision = 20;
epsorder = 4;
auto = SolveIntegrals[target, precision, epsorder];


(*save the results*)
Put[auto, FileNameJoin[{current, "auto"}]];


(*manual evaluation*)
(*set options for auxiliary mass flow*)
(*"WorkingPre": working precision for numerical evaluations, 100 by default*)
(*"XOrder": truncated order of eta expansion during the evaluation, 100 by default*)
SetAMFOptions["WorkingPre" -> 150, "XOrder" -> 300];


(*reduce target integrals to preferred master integrals and compute master integrals with given eps*)
(*structure of soleps: {int1 \[Rule] {int1[eps1], int1[eps2], ...}, int2 \[Rule] {int2[eps1], int2[eps2], ...}, ...}*)
preferred = {};
{masters, rules} = BlackBoxReduce[target, preferred];
epslist = 10^-6 + Range[10] 10^-8;
soleps = BlackBoxAMFlow[masters, epslist];


(*fit the expansion of master integrals and obtain final results for target integrals*)
leading = -4;
values = Transpose@MapThread[(Values[rules]/.eps->#1) . #2&, {epslist, Transpose[masters/.soleps]}];
exp = FitEps[epslist, #, leading]&/@values;
man = Thread[Keys[rules] -> Chop@N[Normal@Series[exp, {eps, 0, 0}], 20]];


(*save the results*)
Put[man, FileNameJoin[{current, "man"}]];


Quit[];
