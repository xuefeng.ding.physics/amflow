(* ::Package:: *)

(* ::Subsection::Closed:: *)
(*begin*)


BeginPackage["AMFlow`"];


(*ibp-interface functions, should be defined in ibp_interface/REDUCER/interface.m*)
SetReducerOptions::usage = "SetReducerOptions[opt] sets options for current ibp reducer.";
IBPSystem::usage = "IBPSystem[top,rank,dot,preferred,dir] sets up ibp system, nothing returned.";
AnalyticReduction::usage = "AnalyticReduction[target] reduces target integrals, a doublet {masters, rules} is returned.";
DifferentialEquation::usage = "DifferentialEquation[inv] sets up the differential equations of master integrals with respect to invariants, a triplet {master, inv, diffeq} is returned.";


AMFlowInfo::usage = "AMFlowInfo[key] returns the global object associated with key used during auxiliary mass flow.";
SaveGlobalVariables::usage = "SaveGlobalVariables[n] saves AMFlowInfo input by user with id=n.";
LoadGlobalVariables::usage = "LoadGlobalVariables[n] loads AMFlowInfo with id=n.";
PrintGlobalVariables::usage = "PrintGlobalVariables[] prints currently used global variables.";


MinusString::usage = "MinusString[n] returns the string with n copies of minus sign -.";
InitializePackage::usage = "InitializePackage[] initializes the package.";
SetAMFOptions::usage = "SetAMFOptions[opt] sets auxiliary mass flow options.";
SetReductionOptions::usage = "SetReductionOptions[opt] sets reduction options.";
PrintOptions::usage = "PrintOptions[func, ctx] prints options for a function.";


RunCommand::usage = "RunCommand[command] executes the command in a new shell.";
GetFile::usage = "GetFile[file] gets mathematica readable file.";
DeleteDir::usage = "DeleteDir[dir] deletes the directory completely.";
CreateDir::usage = "CreateDir[dir] creates the directory.";
ToStringInput::usage = "ToStringInput[exp] transforms exp to string with InputForm.";
StringToTemplate::usage = "StringToTemplate[stringlist] transforms a list of string into a string template.";
SameSetQ::usage = "SameSetQ[set1, set2] tests whether the two sets are the same.";
DynamicPartition::usage = "DynamicPartition[list, part] divides the list into several parts with different length according to part.";
MaximalGroup::usage = "MaximalGroup[matrix] gives the maximal independent set of row vectors.";


ToJ::usage = "ToJ[list] generates a j_integral.";
FromJ::usage = "FromJ[jint] generates the index of a j_integral.";
JSector::usage = "JSector[jint] returns the sector which jint belongs to.";
GetTopSector::usage = "GetTopSector[jints] returns the top sector.";
GetTopPosition::usage = "GetTopPosition[jints] returns the indices for top sector.";
JProp::usage = "JProp[jint] returns the number of propagators.";
JDot::usage = "JDot[jint] returns the number of dots.";
JRank::usage = "JRank[jint] returns the rank.";
IntegralWeight::usage = "IntegralWeight[jint] returns the weight of the integral.";
SortIntegrals::usage = "SortIntegrals[list] sorts a list of integrals by their weight.";
ToSquare::usage = "ToSquare[prop] gives the corresponding momentum and mass, such that prop==momentum^2+mass.";
ToSquareAll::usage = "ToSquareAll[props] returns the momenta and masses.";
GenerateSquare::usage = "GenerateSquare[prop, symbol] transforms a linear propagator like l.p+m to l^2+(l.p+m)/symbol.";
SPList::usage = "a list of complete scalar products construced by loop momenta and external legs.";
SPListSymbol::usage = "a list of symbol, {SP1, SP2, ...}.";
DListSymbol::usage = "a list of symbol, {D1, D2, ...}.";
GetPropagatorMatrix::usage = "GetPropagatorMatrix[prop, splist] returns the coefficient matrix of prop in terms of splist.";
CheckCompleteness::usage = "CheckCompleteness[] checks the completeness of Propagator.";
ToComplete::usage = "ToComplete[de] generates a complete set of denominators in terms of SPListSymbol.";
ToCompleteExplicit::usage = "ToCompleteExplicit[de] generates a complete set of denominators.";
SPListRule::usage = "SPListRule[de] solves SPListSymbol in terms of DListSymbol.";
FeynmanPara::usage = "FeynmanPara[n] returns {x1, x2, ..., xn}.";
EvaluateABC::usage = "EvaluateABC[deno] returns ingredients for computation of graph polynomial.";
EvaluateUF::usage = "EvaluateUF[deno] returns graph polynomial U, F0 and mass term.";
FactorizeFamily::usage = "FactorizeFamily[deno, intpatts] factorizes the integral family specified by deno.";
Vacuum::usage = "Vacuum[m,n] returns the results of m-loop n-propagator single-mass vacuum integral.";


AnalyzeComponent::usage = "AnalyzeComponent[u, f, massterm] gives basic information of the component.";
AnalyzeTopology::usage = "AnalyzeTopology[{u, f, massterm}, cutpara] gives the basic information of the topology.";
AllPossiblePosition::usage = "AllPossiblePosition[info] gives all possible positions to insert eta.";
MassMode::usage = "MassMode[allposi] returns possible choice in mass mode.";
PropagatorMode::usage = "PropagatorMode[allposi] returns possible choice in propagator mode.";
LoopMode::usage = "LoopMode[allposi] returns possible choice in loop mode.";
BranchMode::usage = "BranchMode[allposi] returns possible choice in branch mode.";
AllMode::usage = "AllMode[allposi] returns possible choice in all mode.";
AMFCandidate::usage = "AMFCandidate[info, modefunc] returns possible choices to insert eta.";
AMFPosition::usage = "AMFPosition[topposi, modefunc] returns a possible choice to insert eta.";


BranchToLoop::usage = "BranchToLoop[branch] generates a rule which can transforms loop momenta of branches into standard ones.";
BranchScale::usage = "BranchScale[branches, patt] returns whether momenta of branches are large or small.";
FindAllRegion::usage = "FindAllRegion[topposi] gives all possible retions near the infinity.";
RegionRule::usage = "RegionRule[patt] gives a transformation rule of loop momenta with given pattern.";
RegionPower::usage = "RegionPower[integrals, region] gives the leading power of integrals in the given region.";


FactorSum::usage = "FactorSum[sum] simplifies a summation.";
SimplifyIntegrand::usage = "SimplifyIntegrand[integrand] simplifies a integrand.";
BoundaryIntegrands::usage = "BoundaryIntegrands[integrals,border,region] gives boundary integrands corresponding to given integrals in given given with given boundary orders. ";
ApartRationals::usage = "ApartRationals[rationls] performs partial fraction to a list of rational functions.";
LaportaIntegrals::usage = "LaportaIntegrals[term] transforms a term into Larporta's notation.";
BoundaryIntegrals::usage = "BoundaryIntegrals[completede, integrands] generates boundary integrals.";


$AMFDirectory::usage = "directory for auxiliary mass flow.";
SystemDirectory::usage = "SystemDirectory[sysid] returns the working directory for system with sysid.";
AMFDifferentialEquation::usage = "AMFDifferentialEquation[preferred, sysid] sets up the differential equation.";
AMFBoundaryOrder::usage = "AMFBoundaryOrder[sysid] computes the boundary orders.";
ReduceBoundary::usage = "ReduceBoundary[integrals,border,region] reduces boundary integrals.";
AMFBoundaryCondition::usage = "AMFBoundaryCondition[sysid] reduces the boundary integrals.";
AMFSystem::usage = "AMFSystem[etac,preferred,direction,sysid] sets up a single system.";
AMFSystemMaster::usage = "AMFSystemMaster[preferred,direction,sysid] sets up a single syetem.";
GetNextSystemID::usage = "GetNextSystemID[] returns id of next system in $AMFDirectory.";
AMFAllSystem::usage = "AMFAllSystem[preferred] sets up systems of differential equations and boundary conditions.";
AMFSolution::usage = "AMFSolution[sysid] solves current system.";
AMFAllSolution::usage = "AMFAllSolution[epslist, sysid] evaluates master integrals with given values of eps for the given system.";


CacheDirectory::usage = "CacheDirectory[key] returns the cache directory determined by key.";
BlackBoxReduce::usage = "BlackBoxReduce[jints, jpreferred, dir] performs black-box reduction.";
BlackBoxDiffeq::usage = "BlackBoxDiffeq[{jint1, jint2, ...}, vars, dir] construct differential equations with {jint1, jint2,..} as preferred masters.";
BlackBoxAMFlow::usage = "BlackBoxAMF[{jint1, jint2, ...}, epslist, dir] performs black-box auxiliary mass flow with {jint1, jint2, ...} as preferred masters.";


FitEps::usage = "FitEps[epslist,vlist,leading] fits a Laurant expansion with given interpolations.";
GenerateNumericalConfig::usage = "GenerateNumericalConfig[goal, order] generates a triplet {epslist, workingpre, xorder} for automatic evaluations.";
SolveIntegrals::usage = "SolveIntegrals[jints, goal, order] solves given integrals with given precision goal up to given eps order.";
ExpandGaugeX::usage = "ExpandGaugeX[dir] computes the asymptotic expansion of integrals near gaugex=0, where dir is the working directory.";
SolveIntegralsGaugeLink::usage = "SolveIntegralsGaugeLink[jints, goal, order] solves integrals with gauge link.";


Begin["`Private`"];


(* ::Subsection::Closed:: *)
(*common*)


(* ::Subsubsection::Closed:: *)
(*global variables*)


$PackageInfo = {"master", "DD/MM/YYYY"};


Family := AMFlowInfo["Family"];
Loop := AMFlowInfo["Loop"];
IndepLeg := Select[AMFlowInfo["Leg"], !MemberQ[Keys[AMFlowInfo["Conservation"]], #]&];
SPToSTU:= Module[{complete,keys,values,bare,full},
If[IndepLeg === {}, Return[{}]];
complete = Outer[Times, IndepLeg, IndepLeg]//Flatten//DeleteDuplicates;
keys = Expand[Keys[AMFlowInfo["Replacement"]]/.AMFlowInfo["Conservation"]];
values = Values[AMFlowInfo["Replacement"]];
bare = Coefficient[#, complete]&/@keys;
full = Append[Transpose[bare],values]//Transpose;
If[MatrixRank[bare]<Length[complete], Print["SPToSTU: insufficient replacement rules for all independent external scalar products."]; Abort[]];
If[MatrixRank[full]>Length[complete], Print["SPToSTU: inconsistent replacement rules."]; Abort[]];
Thread[complete -> RowReduce[full][[;;Length[complete], -1]]]
];
Propagator := AMFlowInfo["Propagator"]/.AMFlowInfo["Conservation"];
Cut := AMFlowInfo["Cut"];
IBPRule := AMFlowInfo["Numeric"];
NThread := AMFlowInfo["NThread"];


SaveGlobalVariables[n_]:=Block[{},
$Global[n] = AMFlowInfo/@{"Family", "Loop", "Leg", "Conservation", "Replacement", "Propagator", "Cut", "Numeric", "NThread"};
];
LoadGlobalVariables[n_]:=Block[{},
AMFlowInfo["Family"] = $Global[n][[1]];
AMFlowInfo["Loop"] = $Global[n][[2]];
AMFlowInfo["Leg"] = $Global[n][[3]];
AMFlowInfo["Conservation"] = $Global[n][[4]];
AMFlowInfo["Replacement"] = $Global[n][[5]];
AMFlowInfo["Propagator"] = $Global[n][[6]];
AMFlowInfo["Cut"] = $Global[n][[7]];
AMFlowInfo["Numeric"] = $Global[n][[8]];
AMFlowInfo["NThread"] = $Global[n][[9]];
];


PrintGlobalVariables[]:=Block[{},
Print["PrintGlobalVariables: currently used global variables:"];
Print[# -> AMFlowInfo[#]]&/@{"Family", "Loop", "Leg", "Conservation", "Replacement", "Propagator", "Cut", "Numeric", "NThread"};
];


ep = Symbol["Global`eps"];
et = Symbol["Global`eta"];
$CTX = $Context;
$Current = DirectoryName[$InputFileName];
$DESolverPath = FileNameJoin[{$Current, "diffeq_solver", "DESolver.m"}];
$WolframPath = FileNameJoin[{$InstallationDirectory, "Executables", "wolfram"}];


(* ::Subsubsection::Closed:: *)
(*package*)


MinusString[n_]:=StringJoin@@ConstantArray["-", n];


InitializePackage[]:=Block[{},
Print[MinusString[50]];
Print[StringTemplate["AMFlow: package loaded
Author: Xiao Liu and Yan-Qing Ma
Current version: `ver`
Release date: `date`"][<|"ver" -> $PackageInfo[[1]], "date" -> $PackageInfo[[2]]|>]];
Print[MinusString[50]];
SetAMFOptions@@Options[SetAMFOptions];
SetReductionOptions@@Options[SetReductionOptions];
Print[MinusString[50]];
Print[
"To start, you need to define:
 - AMFlowInfo[\"Family\"] as the name of the family (should be a symbol)
 - AMFlowInfo[\"Loop\"] as a list of loop momenta like {l1, l2}
 - AMFlowInfo[\"Leg\"] as a list of external legs like {p1, p2, p3}
 - AMFlowInfo[\"Conservation\"] as a replacement rule of momentum conservation like {p3 -> -p1-p2}
 - AMFlowInfo[\"Replacement\"] as a list of complete replacement rules for scalar products among external legs like {p1^2 -> 0, p2^2 -> 0, p3^2 -> s} 
 - AMFlowInfo[\"Propagator\"] as a list of complete inverse propagators like {l1^2-msq, l2^2, (l1+l2+p1)^2, ...}
 - AMFlowInfo[\"Cut\"] as a list of cut information like {1, 1, 1, 0, 0, 0, 0}, where '0' means uncut propagators and '1' means cut propagators
 - AMFlowInfo[\"Numeric\"] as a list of replacement rules for involved mass scales like {s -> 100, msq -> 1}
 - AMFlowInfo[\"NThread\"] as the number of threads in use"];
Print[MinusString[50]];
];


Options[SetAMFOptions] = {"AMFMode" -> "MassMode", "WorkingPre" -> 100, "ChopPre" -> 20, "XOrder" -> 100, "ExtraXOrder" -> 20, "LearnXOrder" -> -1, "TestXOrder" -> 5};
SetAMFOptions[opt___]:=Block[{},
If[MemberQ[Keys[{opt}], "AMFMode"], $AMFMode = "AMFMode"/.{opt}];
If[MemberQ[Keys[{opt}], "WorkingPre"], $WorkingPre = "WorkingPre"/.{opt}];
If[MemberQ[Keys[{opt}], "ChopPre"], $ChopPre = "ChopPre"/.{opt}];
If[MemberQ[Keys[{opt}], "XOrder"], $XOrder = "XOrder"/.{opt}];
If[MemberQ[Keys[{opt}], "ExtraXOrder"], $ExtraXOrder = "ExtraXOrder"/.{opt}];
If[MemberQ[Keys[{opt}], "LearnXOrder"], $LearnXOrder = "LearnXOrder"/.{opt}];
If[MemberQ[Keys[{opt}], "TestXOrder"], $TestXOrder = "TestXOrder"/.{opt}];

PrintOptions[SetAMFOptions, $CTX];
];


Options[SetReductionOptions] = {"IBPReducer" -> "FiniteFlow+LiteRed", "BlackBoxRank" -> 3, "BlackBoxDot" -> 0, "DeleteBlackBoxDirectory" -> True};
SetReductionOptions[opt___]:=Block[{},
If[MemberQ[Keys[{opt}], "IBPReducer"], $IBPReducer = "IBPReducer"/.{opt}];
If[MemberQ[Keys[{opt}], "BlackBoxRank"], $BlackBoxRank = "BlackBoxRank"/.{opt}];
If[MemberQ[Keys[{opt}], "BlackBoxDot"], $BlackBoxDot = "BlackBoxDot"/.{opt}];
If[MemberQ[Keys[{opt}], "DeleteBlackBoxDirectory"], $DeleteBlackBoxDirectory = "DeleteBlackBoxDirectory"/.{opt}];

PrintOptions[SetReductionOptions, $CTX];
If[MemberQ[Keys[{opt}], "IBPReducer"], GetFile[FileNameJoin[{$Current, "ibp_interface", $IBPReducer, "interface.m"}]]];
];


PrintOptions[func_, ctx_]:=Print[func -> Thread[Keys[Options[func]] -> ToExpression[ctx<>"$"<>#&/@Keys[Options[func]]]]];


(* ::Subsubsection::Closed:: *)
(*general*)


Options[RunCommand] = {"log" -> False, Sequence@@Options[RunProcess]};
RunCommand[command: _String | _List, OptionsPattern[]] := Module[{opt, time, prop, fout, ferr,log,fp},
opt = #[[1]]->OptionValue@#[[1]]&/@Options[RunProcess];
log = OptionValue@"log";

{time, prop} = AbsoluteTiming[RunProcess[command, Sequence@@opt]];

If[log =!= False,
fp = OpenWrite[log <> ".out"];
WriteString[fp, prop["StandardOutput"]];
Close[fp];
fp = OpenWrite[log <> ".err"];
WriteString[fp, prop["StandardError"]];
Close[fp];
];

If[prop["ExitCode"]=!=0, Print["RunCommand: process failed" -> command]; Abort[]];
time
];


GetFile[file_]:=If[!FileExistsQ[file], Print["GetFile: file not found" -> file]; Abort[], Get[file]];
DeleteDir[dir_]:=If[DirectoryQ[dir], DeleteDirectory[dir, DeleteContents -> True]];
CreateDir[dir_]:=If[!DirectoryQ[dir], CreateDirectory[dir]];


ToStringInput[exp_]:=ToString[exp,InputForm];
StringToTemplate[list_]:=StringTemplate[StringJoin[Riffle[list,"\n\n\n"]]];


SameSetQ[set1_,set2_]:=SubsetQ[set1,set2] && SubsetQ[set2,set1];


DynamicPartition[l_,p_]:=MapThread[l[[#;;#2]]&,{{0}~Join~Most@#+1,#}&@Accumulate@p];


MaximalGroup[matrix_]:= Flatten[FirstPosition[#,1]&/@Select[RowReduce[Transpose@matrix], !AllTrue[#, #===0&]&]];


(* ::Subsubsection::Closed:: *)
(*integral family*)


ToJ[integral_]:=Symbol["j"][Family,Sequence@@integral];
FromJ[jint_]:=List@@jint[[2;;]];


JSector[jint_]:=ToJ[If[#<=0,0,1]&/@FromJ[jint]];


GetTopSector[jints_]:=If[Max[#]>0, _, 0]&/@Transpose[FromJ/@jints];
GetTopPosition[jints_]:=Flatten@Position[GetTopSector[jints], _?(#===_&)];


JProp[jint_]:=Length@Select[FromJ[jint],#>0&];
JDot[jint_]:=Total@Select[FromJ[jint]-1,#>0&];
JRank[jint_]:=-Total@Select[FromJ[jint],#<0&];


IntegralWeight[jint_]:={-JProp[jint], -JDot[jint], -JRank[jint], Min[FromJ[jint]], jint};
SortIntegrals[jints_]:=SortBy[jints, IntegralWeight];


ToSquare[prop_]:=Module[{exp,loop,coe,mom,mass},
exp = Exponent[prop, Loop];
loop = Pick[Loop, exp, 2];
If[loop==={}, Print["ToSquare: nonstandard denominator" -> prop]; Abort[]];
loop = loop[[1]];
coe = Coefficient[prop, loop, {2,1,0}];
{mom, mass} = Expand[Expand[{Sqrt[coe[[1]]]loop+coe[[2]]/(2*Sqrt[coe[[1]]]),coe[[3]]-Power[coe[[2]],2]/(4*coe[[1]])}]/.SPToSTU];
If[Intersection[Variables[mass], Loop]=!={}, Print["ToSquare: cannot make a squared denominator" -> prop]; Abort[]];
{mom, mass}
];
ToSquareAll[proplist_]:=Transpose[ToSquare/@proplist];


GenerateSquare[prop_, symbol_]:=Block[{coerule,linear,factor},
coerule = CoefficientRules[prop, Loop];
If[Max[Total/@Keys[coerule]]===2, Return[prop]];
If[Max[Total/@Keys[coerule]]===0, Print["GenerateSquare: this propagator seems to be a constant :)" -> prop]; Abort[]];
linear = FromCoefficientRules[Select[coerule, Total[Keys[#]]===1&], Loop];
factor = Select[FactorList[linear][[All,1]], Intersection[Variables[#], Loop]=!={} && Intersection[Variables[#], IndepLeg]==={}&];
If[Length[factor]=!=1, Print["GenerateSquare: cannot identify this denominator" -> prop]; Abort[]];
factor[[1]]^2+prop/symbol
];


SPList:=Join[DeleteDuplicates[Join@@Outer[Times, Loop, Loop]], Join@@Outer[Times, Loop, IndepLeg]];
SPListSymbol:=Symbol/@("SP"<>ToStringInput@#&/@Range[Length@SPList]);
DListSymbol:=Symbol/@("D"<>ToStringInput@#&/@Range[Length@SPList]);


GetPropagatorMatrix[prop_, splist_]:=Coefficient[#, splist]&/@prop;


CheckCompleteness[]:=Block[{rank},
rank = MatrixRank@GetPropagatorMatrix[Propagator, SPList];
If[rank < Length[SPList], Print["CheckCompleteness: propagators set is not complete. Try using ToCompleteExplicit to make it complete."]; Abort[]];
];


ToComplete[de_]:=Module[{pde,all,index},
If[Length[Loop]===0, Return[{}]];
pde = Power[Join[DeleteDuplicates[Join@@Outer[Plus,Loop,Loop]/.(2a_:>a)],Join@@Outer[Plus,Loop,IndepLeg]],2];
all = Expand@Join[de, pde]/.SPToSTU/.Thread[SPList->SPListSymbol];
index = MaximalGroup[GetPropagatorMatrix[all, SPListSymbol]];
all[[index]]
];


ToCompleteExplicit[de_]:=ToComplete[de]/.Thread[SPListSymbol -> SPList];


SPListRule[complete_]:=Solve[Thread[complete==DListSymbol],SPListSymbol][[1]];


FeynmanPara[n_]:=Symbol/@("x"<>ToStringInput@#&/@Range[n]);


EvaluateABC[denominator_]:=Module[{n,l,e,momentum,A,B,C,x,Ax,Bx,Cx},
n = Length@denominator;
l = Length@Loop;
e = Length@IndepLeg;
momentum = Join[Loop, IndepLeg];
Table[
A[i]=Table[Which[j==k,Coefficient[denominator[[i]]//Expand,Loop[[j]]^2],True,1/2 Coefficient[denominator[[i]]//Expand,Loop[[j]]Loop[[k]]]],{j,l},{k,l}];
B[i]=Table[-1/2 Coefficient[denominator[[i]]//Expand,Loop[[j]]IndepLeg[[k]]],{j,l},{k,e}] . IndepLeg;
C[i]=denominator[[i]]/.Thread[Loop->ConstantArray[0,l]],{i,n}];
Ax = Total[FeynmanPara[n]Array[A,n]];
Bx = Total[FeynmanPara[n]Array[B,n]];
Cx = Total[FeynmanPara[n]Array[C,n]];
{Ax,Bx,Cx}
];


EvaluateUF[denominator_]:=Module[{Ax,Bx,Cx,u,f,f0},
{Ax,Bx,Cx} = EvaluateABC[denominator];
u = Expand[Det[Ax]];
f0 = -ToSquareAll[denominator][[2]] . FeynmanPara[Length[denominator]];
f = Expand[Together[Expand[u(- Cx+ Bx . Inverse[Ax] . Bx)]/.SPToSTU]-u*f0];
{u,f,f0}
];


FactorizeFamily[denom_, patts_]:=Module[{u,f,massterm,info,index,u0,vacQ,var,mass,mono,tobeloop,mat,x,solved,redef},
{u, f, massterm} = EvaluateUF[denom];
info = AnalyzeTopology[{u, f, massterm}, {}];
info = Select[info, #[[3]]>0&];
index = PositionIndex[FeynmanPara[Length[denom]]];

Table[
{u0, vacQ, var, mass} = info[[i, {1, 2, 4, 5}]];
mono = If[Head[u0]===Plus, List@@u0, {u0}];
If[vacQ===True && Count[mass, 1]===1 && Count[mass, 0]===Length[mass]-1, 
mono = SortBy[mono, !MemberQ[Variables[#], Pick[var, mass, 1][[1]]]&]];
tobeloop = Flatten[Variables[mono[[1]]]/.index];
mat = Coefficient[#, Loop]&/@ToSquareAll[denom[[tobeloop]]][[1]];
mat = RowReduce[Append[mat[[#]], -x[#]]&/@Range[Length[mat]]];
solved = FirstPosition[#, 1]&/@mat//Flatten;
redef = Table[Loop[[solved[[k]]]] -> Loop[[solved[[k]]]]-mat[[k]] . Append[Loop, 1], {k, Length[solved]}];
redef = Thread[Keys[redef] -> (Values[redef]/.Thread[x/@Range[Length[mat]] -> Keys[redef]])];
{Keys[redef], Expand[denom[[Flatten[var/.index]]]/.redef], patts[[All, Flatten[var/.index]]]}, {i, Length[info]}]
];


Vacuum[1, 1] := -Gamma[-1 + ep];
Vacuum[2, 3] := -Gamma[1 - ep]^2*Gamma[ep]*Gamma[-1 + 2*ep]/Gamma[2 - ep];
Vacuum[3, 4] := Gamma[1 - ep]^3*Gamma[-1 + 2*ep]*Gamma[-2 + 3*ep]/Gamma[2 - ep];
Vacuum[3, 5] := -Gamma[2 - 3*ep]*Gamma[1 - ep]^4*Gamma[ep]^2*Gamma[-1 + 3*ep]/(Gamma[2 - 2*ep]^2*Gamma[2 - ep]);
Vacuum[4, 5] := -Gamma[1 - ep]^4*Gamma[-2 + 3*ep]*Gamma[-3 + 4*ep]/Gamma[2 - ep];
Vacuum[a___] := (Print["Vacuum: undefined single-mass vacuum integral" -> {a}]; Abort[];);


(* ::Subsubsection::Closed:: *)
(*scheme for eta*)


AnalyzeComponent[u_,f_,massterm_]:=Module[{var,loopnum,mass,fterms,vactest,vacQ},
var = Sort@Variables[u];
loopnum = CoefficientRules[u, var][[1,1]]//Total;
mass = Coefficient[massterm, var];
fterms = If[Head[#]===Plus, List@@#, {#}]&@Expand[f];
vactest = Table[If[Head@fterms[[i]]=!=Times, True, Length@Intersection[List@@fterms[[i]], var] < loopnum+1], {i, Length@fterms}];
vacQ = And@@vactest;
{vacQ, loopnum, var, mass}
];


AnalyzeTopology[{u_,f_,massterm_},cutpara_]:=Module[{components,info},
components = If[Head[#]===Times, List@@#, {#}]&@Factor[u];
info = {#, Sequence@@AnalyzeComponent[#, f, massterm]}&/@components;
info = Sort[info, #1[[3]]>=#2[[3]]&];
info = Append[#, Intersection[cutpara, #[[4]]]]&/@info;
info
];


AllPossiblePosition[info_]:=Module[{u,vacQ,loopnum,var,mass,cutvar,subs,findloop,loops,findbranch,branches,getmass,pros,massivepros},
{u, vacQ, loopnum, var, mass, cutvar} = info;

subs = Times@@@Subsets[var, {loopnum-1}];
findloop[sub_]:=Sort[Select[If[Head[u]===Plus, List@@u, {u}]/sub, PolynomialQ]];
loops = DeleteDuplicates@Select[findloop/@subs, #=!={}&];
loops = Select[loops, Intersection[#, cutvar]==={}&];
loops = Sort[loops, Length@#1<=Length@#2&];

findbranch[v_]:=Sort[Complement[var, Variables[Coefficient[u, v, 1]]]];
branches = DeleteDuplicates[findbranch/@var];
branches = Select[branches, Intersection[#, cutvar]==={}&];
branches = Sort[branches, Length@#1<=Length@#2&];

pros = Sort[Complement[var, cutvar], Length[findbranch[#1]]>=Length[findbranch[#2]]&];

Table[getmass[var[[i]]] = mass[[i]], {i, Length@var}];
massivepros = Select[pros, getmass[#]=!=0&];
massivepros = GatherBy[massivepros, getmass];
massivepros = SortBy[massivepros, MemberQ[Variables[Values[SPToSTU]], getmass[#[[1]]]]&];

{loops, branches, pros, massivepros}
];


MassMode[{loops_,branches_,pros_,massivepros_}]:=Which[
massivepros=!={}, massivepros[[1]],
True, PropagatorMode[{loops, branches, pros, massivepros}]];


PropagatorMode[{loops_,branches_,pros_,massivepros_}]:=Which[
pros=!={}, {pros[[1]]},
True, Nothing[]];


LoopMode[{loops_,branches_,pros_,massivepros_}]:=Which[
loops=!={}, loops[[1]],
True, MassMode[{loops, branches, pros, massivepros}]];


BranchMode[{loops_,branches_,pros_,massivepros_}]:=Which[
branches=!={}, branches[[1]],
True, MassMode[{loops, branches, pros, massivepros}]];


AllMode[{loops_,branches_,pros_,massivepros_}]:=Which[
pros=!={}, pros,
True, Nothing[]];


AMFCandidate[info_, modefunc_]:=Module[{u,vacQ,loopnum,var,mass,cutvar,loops,branches,pros,massivepros},
{u, vacQ, loopnum, var, mass, cutvar} = info;
{loops, branches, pros, massivepros} = AllPossiblePosition[info];

Which[
!(cutvar === {} && vacQ === True), 
modefunc[{loops, branches, pros, massivepros}],

!(Count[mass,1]===1 && Count[mass,0]===Length@mass-1),
branches[[1]],

True,
Nothing[]
]
];


AMFPosition[topposi_, modefunc_]:=Module[{u,f,massterm,cutpara,info,candidates,etac},
If[topposi==={}, Return[{}]];
{u, f, massterm} = EvaluateUF[Propagator[[topposi]]];
cutpara = Pick[FeynmanPara[Length[topposi]], Cut[[topposi]], 1];
info = AnalyzeTopology[{u, f, massterm}, cutpara];
candidates = AMFCandidate[#, modefunc]&/@info;
etac = ConstantArray[0, Length[Propagator]];
If[candidates=!={}, etac[[topposi]] = etac[[topposi]] - Coefficient[Total[candidates[[1]]], FeynmanPara[Length@topposi]]];
etac
];


(* ::Subsubsection::Closed:: *)
(*integration regions*)


BranchToLoop[candidate_]:=If[Det@#===0, $Failed, Thread[Loop->Inverse[#] . Loop]]&@Normal[CoefficientArrays[candidate, Loop]][[2]];


BranchScale[branch_, scale_]:=Table[If[And@@(FreeQ[branch[[i]], #]&/@Pick[Loop, scale, 1]), 0, 1], {i, Length@branch}];


FindAllRegion[topposi_]:=Module[{branch0,branch,cutbranch,cutposi,trans,patt,regions},
branch0 = ToSquareAll[Propagator][[1]]/.Thread[IndepLeg -> 0];
branch = DeleteDuplicates[branch0[[topposi]]];
cutbranch = Pick[branch0, Cut, 1];
cutposi = Flatten@Table[Position[Expand[branch-cutbranch[[i]]], 0], {i, Length@cutbranch}];
trans = Select[BranchToLoop/@Subsets[branch, {Length@Loop}], #=!=$Failed&];
patt = Tuples[{0, 1}, Length@Loop];
regions = Join@@Table[{trans[[i]], patt[[j]], BranchScale[branch/.trans[[i]], patt[[j]]]}, {i, Length@trans}, {j, Length@patt}];
regions = GatherBy[regions, Last][[All, 1]];
regions = Select[regions, AllTrue[#[[-1, cutposi]], #===0&]&][[All, ;;2]];
regions
];


RegionRule[patt_]:=Thread[Loop->Power[et, patt/2]Loop];


RegionPower[integrals_, region_]:=Module[{trans, scale, fullde, factor, powers, p, behavior, beh},
{trans, scale} = region;
fullde = Expand[Propagator/.trans/.RegionRule[scale]];
factor = If[FreeQ[#, et], 1, et]&/@fullde;
powers = p/@Range[Length@Propagator];
behavior = Expand[(2-ep)Total[scale]-Total@Pick[powers, factor, et]];
beh[mi_]:=behavior/.Thread[powers -> FromJ[mi]];
beh/@integrals
];


(* ::Subsubsection::Closed:: *)
(*boundary integrals*)


FactorSum[exp_Plus]:=Module[{gcd},
gcd = PolynomialGCD@@exp;
{gcd, Plus@@(Factor[List@@exp/gcd])}
];


Attributes[SimplifyIntegrand] = {Listable};
SimplifyIntegrand[int_]:=Module[{factors,sums,others,factorsum},
factors = If[Head[int]===Times, List@@int, {int}];
sums = Select[factors, Head[#]===Plus&];
others = Times@@Select[factors, Head[#]=!=Plus&];
factorsum = FactorSum/@sums;
Factor[Times@@factorsum[[All,1]]*others]*Times@@factorsum[[All,2]]
];


BoundaryIntegrands[integrals_,border_,region_]:=Module[{trans,scale,fullde,factor,expde,completede,sptoD,structure,rules,x,rulex,rules0,powers,p,integrand,behavior,int,integrands},
{trans, scale} = region;
fullde = Expand[Propagator/.trans/.RegionRule[scale]];
factor = If[FreeQ[#,et], 1, et]&/@fullde;
fullde = Expand[fullde/factor]/.SPToSTU/.Thread[SPList->SPListSymbol];
expde = Coefficient[fullde, et, 0];
completede = ToComplete[expde[[GetTopPosition[integrals]]]];
sptoD = SPListRule[completede];
fullde = Expand[fullde/.sptoD];
expde = Expand[expde/.sptoD];

structure = Coefficient[#, et, {0, -1/2, -1}]&/@fullde;
rules = Thread[Flatten[Table[x[i, j], {i, Length@expde}, {j, 3}]] -> Flatten[structure]];
rulex = Flatten[If[Length@#===1, {}, Thread[#[[2;;]] -> #[[1]]]]&/@(Keys/@GatherBy[rules, Values])];
rules0 = Select[rules, #[[2]]===0&];

powers = p/@Range[Length@expde];
integrand = Times@@Table[Power[x[i, 1]+x[i, 2]Power[et, -1/2]+x[i, 3]/et, -p[i]], {i, Length@expde}]/.rules0/.rulex;
integrand = Together[SeriesCoefficient[integrand, {et, Infinity, #}]&/@Range[0, Max[border]]];
integrand = integrand/.rules;

int[k_]:=integrand[[;;border[[k]]+1]]/.Thread[powers -> FromJ[integrals[[k]]]];
integrands = int/@Range[Length[integrals]];

{completede, SimplifyIntegrand[integrands]}
];


ApartRationals[rationals_]:=Module[{apart},
apart[rat_List,var_]:=Module[{num,de,apartde},
num = Numerator@rat;
de = Denominator@rat;
apartde = Apart[1/de, var];
Flatten[num(If[Head@#===Plus, List@@#, {#}]&/@apartde)]
];
Fold[apart, {#}, DListSymbol]&/@rationals
];


LaportaIntegrals[term_]:=Module[{ruled},
ruled = CoefficientRules[term//Denominator, DListSymbol];
If[Length@ruled=!=1, Print["LaportaIntegrals: denominator is not a monomial of DListSymbol" -> term]; Abort[]];
ruled[[1,1]]-#[[1]]->#[[2]]/ruled[[1,2]]&/@CoefficientRules[term//Numerator, DListSymbol]
];


BoundaryIntegrals[completede_,integrands_]:=Module[{finddiff,analyze,familyde,partition,integrands2,apartdes,family,familyrule,families,integrals},
finddiff[list_,di_]:=Module[{tmp},
tmp = Cases[list, Plus[di, a_]:>a];
If[tmp==={}, 0, tmp[[1]]]
];

analyze[term_]:=Module[{factors},
factors = FactorList[Denominator[term]][[All,1]];
finddiff[factors, #]&/@DListSymbol
];

familyde[fam_]:=ReplaceAll[completede,Thread[SPListSymbol->SPList]] + fam;

partition = Length/@integrands;
integrands2 = Join@@integrands;
apartdes = ApartRationals[1/Denominator[integrands2]];
apartdes = Together[Table[Plus@@@GatherBy[apartdes[[i]], analyze], {i, Length@apartdes}]];
integrands2 = Numerator[integrands2]*apartdes;

Table[
family = analyze[integrands2[[i, j]]];
familyrule = Thread[DListSymbol->DListSymbol-family];
integrands2[[i, j]] = {family, LaportaIntegrals[integrands2[[i, j]]/.familyrule]}, 
{i, Length@integrands2}, {j, Length@integrands2[[i]]}];

families = DeleteDuplicates[Join@@integrands2[[All, All, 1]]];
integrals[i_, fam_]:={};
Table[integrals[i, integrands2[[i, j, 1]]] = integrands2[[i, j, 2]], {i, Length@integrands2}, {j, Length@integrands2[[i]]}];

Table[{familyde@families[[i]], DynamicPartition[integrals[#, families[[i]]]&/@Range[Length@integrands2], partition]}, {i, Length@families}]
];


(* ::Subsection::Closed:: *)
(*auxiliary mass flow*)


(* ::Subsubsection::Closed:: *)
(*differential equations*)


SystemDirectory[sysid_]:=FileNameJoin[{$AMFDirectory, ToStringInput[sysid]}];


AMFDifferentialEquation[preferred_, sysid_]:=Put[BlackBoxDiffeq[preferred, {et}, FileNameJoin[{$AMFDirectory, ToString[sysid]<>"_diffeq"}]], FileNameJoin[{SystemDirectory[sysid], "diffeq"}]];


(* ::Subsubsection::Closed:: *)
(*boundary conditions*)


AMFBoundaryOrder[sysid_]:=Module[{name,template,rule,time},
name = FileNameJoin[{SystemDirectory[sysid], "bc/border"}];
template = StringToTemplate[{
"If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName//SetDirectory;
Get[`desolver`];
SetDefaultOptions[];
SetExpansionOptions[\"ExtraXOrder\" -> `exorder`];
If[!FileExistsQ[\"../diffeq\"], Print[\"error: diffeq not found.\"]; Abort[]];
{masters, variables, diffeq} = Get[\"../diffeq\"];
powers = Get[\"powers\"];",
"epsrule = {eps \[Rule] 10^-4};
deinf = Together[-Power[eta,-2](diffeq[[1]]/.epsrule/.eta -> 1/eta)];
powers = -powers/.epsrule;
orders = DetermineBoundaryOrder[deinf, #]&/@powers;
porders = If[#<0, 0, #]&/@Max/@Transpose[orders];",
"Put[porders, \"border\"];
Quit[];"}];
rule = <|"desolver" -> ToStringInput[$DESolverPath], "exorder" -> ToStringInput[$ExtraXOrder]|>;
FileTemplateApply[template, rule, name<>".wl"];
Print[StringTemplate["AMFBoundaryOrder: determining boundary orders for system `1`."][ToStringInput[sysid]]];
time = RunCommand[{$WolframPath, "-noprompt", "-script", name<>".wl"}, "log" -> name<>".log"];
Print[StringTemplate["AMFBoundaryOrder: boundary orders determined in `1`s."][ToStringInput[Ceiling[time]]]];
GetFile[name]
];


ReduceBoundary[integrals_, border_, region_]:=Module[{boundary,prop,allintegrals,cutde,cut,thisid,masters,rules,f,table,str,coe},
boundary = BoundaryIntegrals@@BoundaryIntegrands[integrals, border, region];

Table[
prop = boundary[[i, 1]];
allintegrals = DeleteDuplicates[Keys/@Flatten[boundary[[i,2]],2]];
cutde = Pick[Propagator/.region[[1]], Cut, 1];
cut = If[MemberQ[Expand[#-cutde]/.SPToSTU, 0], 1, 0]&/@prop;
If[Count[cut, 1]=!=Count[Cut, 1], Print["ReduceBoundary: eta may have been inserted to cut denominators."]; Abort[]];

SaveGlobalVariables[thisid];
AMFlowInfo["Propagator"] = prop;
AMFlowInfo["Cut"] = cut;
{masters, rules} = BlackBoxReduce[ToJ/@allintegrals, {}, FileNameJoin[{$AMFDirectory, "bc_reduce"}]];
LoadGlobalVariables[thisid];

ClearAll[f];
f[___]:=ConstantArray[0, Length@masters];
Table[f[FromJ@rules[[k, 1]]] = rules[[k, 2]], {k, Length@rules}];

table = Table[
str = boundary[[i, 2, j, k]];
coe = Together[Total[f[#[[1]]]*#[[2]]&/@str]/.IBPRule];
If[coe===0, coe = ConstantArray[0, Length@masters]];
coe, {j, Length@boundary[[i,2]]}, {k,Length@boundary[[i,2,j]]}];

{prop, cut, FromJ/@masters, table}, {i, Length@boundary}]
];


AMFBoundaryCondition[sysid_]:=Module[{time1,dir,demi,regions,powers,border,reduce,boundaries,time2},
Print[StringTemplate["AMFBoundaryCondition: preparing boundary integrals for system `1`."][ToStringInput[sysid]]];
time1 = AbsoluteTime[];
dir = FileNameJoin[{SystemDirectory[sysid], "bc"}];
CreateDir[dir];

demi = GetFile[FileNameJoin[{SystemDirectory[sysid], "diffeq"}]][[1]];
regions = FindAllRegion[GetTopPosition[demi]];
Print[StringTemplate["AMFBoundaryCondition: `1` possible integration regions near the Infinity."][ToStringInput[Length[regions]]]];
powers = RegionPower[demi, #]&/@regions;
Put[powers, FileNameJoin[{dir, "powers"}]];
border = AMFBoundaryOrder[sysid];

Table[
Print["AMFBoundaryCondition: region "<>ToStringInput[i] -> Thread[Keys[regions[[i, 1]]] -> (Values[regions[[i,1]]]/.RegionRule[regions[[i,2]]])]];
reduce[i] = ReduceBoundary[demi, border, regions[[i]]];
Put[reduce[i], FileNameJoin[{dir, "r"<>ToStringInput[i]}]];
Print["AMFBoundaryCondition: region "<>ToStringInput[i]<>" finished."], {i, Length[regions]}];

boundaries = reduce/@Range[Length[regions]];
boundaries = Join@@Table[{powers[[i]], boundaries[[i, j]]}, {i, Length@powers}, {j, Length@boundaries[[i]]}];
boundaries = Select[boundaries, #[[2,3]]=!={}&];
Put[boundaries, FileNameJoin@{dir, "all"}];
time2 = AbsoluteTime[];
Print[StringTemplate["AMFBoundaryCondition: boundary integrals prepared in `1`s."][ToStringInput[Ceiling[time2-time1]]]];
];


(* ::Subsubsection::Closed:: *)
(*set up*)


AMFSystem[etac_, preferred_, direction_, sysid_]:=Module[{time1,time2,thisid},
Print[MinusString[30]];
Print[StringTemplate["AMFSystem: setting up system `1`."][ToStringInput[sysid]]];
time1 = AbsoluteTime[];

CreateDir[SystemDirectory[sysid]];
If[Length[Loop]===0, 
Print["AMFSystem: ending system."];
Put[Null, FileNameJoin[{SystemDirectory[sysid], "ending"}]],
SaveGlobalVariables[thisid];
Put[$Global[thisid], FileNameJoin[{SystemDirectory[sysid], "config"}]];
AMFlowInfo["Propagator"] = Propagator + et*etac;
PrintGlobalVariables[];
AMFDifferentialEquation[preferred, sysid];
Print[MinusString[15]];
AMFBoundaryCondition[sysid];
Put[direction, FileNameJoin[{SystemDirectory[sysid], "direction"}]];
Put[{preferred, ConstantArray[1, Length[preferred]], preferred, Identity}, FileNameJoin[{SystemDirectory[sysid], "preferred"}]];
LoadGlobalVariables[thisid]];

time2 = AbsoluteTime[];
Print[StringTemplate["AMFSystem: system set up in `1`s."][ToStringInput[Ceiling[time2-time1]]]];
];


AMFSystemMaster[preferred_, direction_, sysid_]:=Module[{posi,u,f,massterm,cutpara,info,cutcom,etac,prefactor,faminfo,thisid,loop,prop,mas,drop,droploop,jpre,m,n},
posi = GetTopPosition[preferred];
{u, f, massterm} = EvaluateUF[Propagator[[posi]]];
cutpara = Pick[FeynmanPara[Length@posi], Cut[[posi]], 1];
info = AnalyzeTopology[{u, f, massterm}, cutpara];
info = Select[info, #[[3]]>0&];
cutcom = Select[info, SameSetQ[#[[4]], #[[6]]]&];
etac = AMFPosition[posi, ToExpression[$AMFMode]];

If[Length[cutcom]>1, Print["AMFSystemMaster: too many cut components" -> cutcom]; Abort[]];
If[!AllTrue[etac, #===0&], AMFSystem[etac, preferred, direction, sysid]; Return[{sysid}]];
If[Length[cutcom]===1,
SaveGlobalVariables[thisid];
AMFlowInfo["Cut"] = Cut/.{1 -> 0};
etac = AMFPosition[posi, ToExpression[$AMFMode]];
AMFSystem[etac, preferred, direction, sysid];
prefactor = ConstantArray[2 (Pi^(2-ep)/(2Pi)^(4-2ep))^cutcom[[1,3]] (-1)^(cutcom[[1,3]]+1), Length[preferred]];
Put[{preferred, prefactor, preferred, Im}, FileNameJoin[{SystemDirectory[sysid], "preferred"}]];
LoadGlobalVariables[thisid];
Return[{sysid}]];

If[AnyTrue[Join@@(FromJ/@preferred), #<0&], Print["AMFSystem: unexpected pattern of single-mass vacuum master integrals" -> preferred]; Abort[]];
faminfo = FactorizeFamily[Propagator[[posi]]/.Thread[IndepLeg -> 0], FromJ[#][[posi]]&/@preferred];

Table[
SaveGlobalVariables[thisid];
{loop, prop, mas} = faminfo[[i]];
drop = Position[ToSquareAll[prop][[2]], -1][[1, 1]];
droploop = ToSquare[prop[[drop]]][[1]];
AMFlowInfo["Loop"] = Select[loop, #=!=droploop&];
AMFlowInfo["Leg"] = {droploop};
AMFlowInfo["Conservation"] = {};
AMFlowInfo["Replacement"] = {droploop^2 -> 1};
AMFlowInfo["Propagator"] = ToCompleteExplicit[Drop[prop, {drop}]];
AMFlowInfo["Cut"] = Table[0, Length[Propagator]];
AMFlowInfo["Numeric"] = {};
jpre = ToJ[PadRight[Drop[#, {drop}], Length[Propagator]]]&/@mas;
prefactor = Simplify[((-1)^(-m-n)*Gamma[2-ep-m]*Gamma[-2+ep+m+n])/(Gamma[2-ep]*Gamma[n])/.{n -> mas[[All, drop]], m -> Total/@mas-mas[[All, drop]]-(Length[loop]-1)*(2-ep)}];
etac = AMFPosition[GetTopPosition[jpre], ToExpression[$AMFMode]];
AMFSystem[etac, DeleteDuplicates[jpre], direction, sysid+i-1];
Put[{preferred, prefactor, jpre, Identity}, FileNameJoin[{SystemDirectory[sysid+i-1], "preferred"}]];
LoadGlobalVariables[thisid], {i, Length[faminfo]}];

sysid+Range[Length[faminfo]]-1
];


GetNextSystemID[]:=Module[{i = 1}, While[DirectoryQ[SystemDirectory[i]], i++]; i];


AMFAllSystem[preferred_]:=Module[{sysids,thisid,thisid2,boundary,subid},
If[!MemberQ[{"MassMode", "PropagatorMode", "BranchMode", "LoopMode", "AllMode"}, $AMFMode], 
Print["AMFAllSystem: undefined AMFMode, use default mode instead."];
$AMFMode = "AMFMode"/.Options[SetAMFOptions]];
If[preferred==={}, Print["AMFAllSystem: no preferred masters input."]; Abort[]];

sysids = AMFSystemMaster[preferred, "NegIm", GetNextSystemID[]];

SaveGlobalVariables[thisid];
Table[
If[!FileExistsQ@FileNameJoin[{SystemDirectory[sysid], "ending"}],
$Global[thisid2] = GetFile[FileNameJoin[{SystemDirectory[sysid], "config"}]];
LoadGlobalVariables[thisid2];
boundary = GetFile[FileNameJoin[{SystemDirectory[sysid], "bc/all"}]];
subid = {};
Table[
AMFlowInfo["Propagator"] = boundary[[i,2,1]];
AMFlowInfo["Cut"] = boundary[[i,2,2]];
AppendTo[subid, AMFAllSystem[ToJ/@boundary[[i,2,3]]]], {i, Length@boundary}];
Put[subid, FileNameJoin[{SystemDirectory[sysid], "bc/sub_id"}]]], {sysid, sysids}];
LoadGlobalVariables[thisid];

sysids
];


(* ::Subsubsection::Closed:: *)
(*solve*)


AMFSolution[sysid_]:=Module[{name,template,rule,time},
name = FileNameJoin[{SystemDirectory[sysid], "nsol/solve"}];
template = StringToTemplate[{
"If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName//SetDirectory;
Get[`desolver`];
{masters, variables, diffeq} = Get[\"../diffeq\"];
boundary = Get[\"../bc/all\"];
direction = Get[\"../direction\"];
boundaryMI = Get[\"boundarymi\"];",
"sol[n_]:=Module[{epsrule, evaluate, tobcs, bmi, de, bc},
SetDefaultOptions[];
SetGlobalOptions[\"WorkingPre\" -> `wkpre`, \"ChopPre\" -> `cppre`];
SetExpansionOptions[\"XOrder\" -> `xorder`, \"ExtraXOrder\" -> `exorder`, \"LearnXOrder\" -> `lorder`, \"TestXOrder\" -> `torder`];
SetRunningOptions[\"RunDirection\" -> direction];
evaluate[tab_,list_]:=Map[#.list&,tab/.epsrule,{2}];
tobcs[beh_,tab_]:=MapThread[Thread[#1-Range[0,Length@#2-1]->#2]& ,{beh/.epsrule,tab}];
epsrule = {eps -> boundaryMI[[n,1]]};
bmi = Map[N[#, $MinPrecision]&, boundaryMI[[n,2]], {2}];
de = Together[diffeq[[1]]/.epsrule];
bc = MapThread[tobcs[#1, evaluate[#2, #3]]&, {boundary[[All,1]], boundary[[All,2,-1]], bmi}];
bc = Join@@@Transpose[bc];
AMFlow[de, bc]];",
"LaunchKernels[`kernel`];
results = ParallelTable[sol[i], {i, Length[boundaryMI]}, DistributedContexts -> All];
CloseKernels[];",
"Put[Thread[masters -> Transpose[results]], \"sol\"];",
"Quit[];"}];

rule = <|
"desolver" -> ToStringInput[$DESolverPath],
"wkpre" -> ToStringInput[$WorkingPre],
"cppre" -> ToStringInput[$ChopPre],
"xorder" -> ToStringInput[$XOrder],
"exorder" -> ToStringInput[$ExtraXOrder],
"lorder" -> ToStringInput[$LearnXOrder],
"torder" -> ToStringInput[$TestXOrder],
"kernel" -> ToStringInput[NThread]
|>;

FileTemplateApply[template, rule, name<>".wl"];
Print[MinusString[30]];
Print[StringTemplate["AMFSolution: solving system `1`."][ToStringInput[sysid]]];
time = RunCommand[{$WolframPath, "-noprompt", "-script", name<>".wl"}, "log"->name<>".log"];
Print[StringTemplate["AMFSolution: system solved in `1`s."][ToStringInput[Ceiling[time]]]];
GetFile[FileNameJoin[{SystemDirectory[sysid], "nsol/sol"}]]
];


AMFAllSolution[eplist_, sysids_]:=Module[{all,preferred,coe,prelocal,op,boundary,subids,mivalue,rule,jmis,nbmi,sol,values},
all = Table[
{preferred, coe, prelocal, op} = GetFile[FileNameJoin[{SystemDirectory[sysid], "preferred"}]];
If[!FileExistsQ[FileNameJoin[{SystemDirectory[sysid], "ending"}]],
CreateDir[FileNameJoin[{SystemDirectory[sysid], "nsol"}]];
boundary = GetFile[FileNameJoin[{SystemDirectory[sysid], "bc/all"}]];
subids = GetFile[FileNameJoin[{SystemDirectory[sysid], "bc/sub_id"}]];
mivalue = {};
Table[
jmis = ToJ/@boundary[[i,2,3]];
rule = AMFAllSolution[eplist, subids[[i]]];
If[jmis=!=Keys[rule], Print["AMFAllSolution: unmatched boundary integrals."]; Abort[]];
AppendTo[mivalue, Values[rule]], {i, Length@boundary}];
nbmi = Table[eplist[[i]] -> mivalue[[All,All,i]], {i, Length@eplist}];
Put[nbmi, FileNameJoin[{SystemDirectory[sysid], "nsol/boundarymi"}]];
AMFSolution[sysid];
sol = GetFile[FileNameJoin[{SystemDirectory[sysid], "nsol/sol"}]];
If[!SubsetQ[Keys[sol], prelocal], Print["AMFAllSolution: some boundary integrals have not been solved yet."]; Abort[]],
sol = Thread[prelocal -> ConstantArray[1, {Length[prelocal], Length[eplist]}]]];
Thread[preferred -> N[coe/.ep -> eplist, $WorkingPre]*op[prelocal/.sol]], {sysid, sysids}];

If[DeleteDuplicates@Flatten[Outer[SameQ, Keys[all], Keys[all], 1]]=!={True}, Print["AMFAllSolution: unmatched systems" -> sysids]; Abort[]];
Thread[Keys[all[[1]]] -> Times@@Values[all]]
];


(* ::Subsection::Closed:: *)
(*black-box functions*)


(* ::Subsubsection::Closed:: *)
(*basic*)


CacheDirectory[key_]:=FileNameJoin[{If[$FrontEnd===Null, $InputFileName, NotebookFileName[]]//DirectoryName, "cache", ToStringInput[Family]<>"_"<>key}];


BlackBoxReduce[jints_, jpreferred_, dir_:Unevaluated[CacheDirectory["reduce"]]]:=Block[{top,rank,dot,res},
If[jints === {}, Return[{{}, {}}]];
top = GetTopSector[Join[jints, jpreferred]];
rank = Max[$BlackBoxRank, JRank/@Join[jints, jpreferred]];
dot = Max[$BlackBoxDot, JDot/@Join[jints, jpreferred]];
IBPSystem[top, rank, dot, jpreferred, dir];
res = AnalyticReduction[jints];
If[$DeleteBlackBoxDirectory===True, DeleteDir[dir]];
res
];


BlackBoxDiffeq[jpreferred_, vars_, dir_:Unevaluated[CacheDirectory["diffeq"]]]:=Block[{top,rank,dot,res},
If[jpreferred === {}, Print["BlackBoxDiffeq: no preferred masters input."]; Abort[]];
top = GetTopSector[jpreferred];
rank = Max[$BlackBoxRank, JRank/@jpreferred];
dot = Max[$BlackBoxDot, JDot/@jpreferred+1];
IBPSystem[top, rank, dot, jpreferred, dir];
res = DifferentialEquation[vars];
If[$DeleteBlackBoxDirectory===True, DeleteDir[dir]];
res
];


BlackBoxAMFlow[jpreferred_, epslist_, dir_:Unevaluated[CacheDirectory["amflow"]]]:=Block[{$AMFDirectory,sysid,sol,time1,time2},
$AMFDirectory = dir;
Print["BlackBoxAMFlow: constructing systems."];
time1 = AbsoluteTime[];
sysid = AMFAllSystem[jpreferred];
time2 = AbsoluteTime[];
Print[StringTemplate["BlackBoxAMFlow: systems constructed in `1`s."][ToStringInput[Ceiling[time2-time1]]]];

Print["BlackBoxAMFlow: solving systems."];
time1 = AbsoluteTime[];
sol = AMFAllSolution[epslist, sysid];
time2 = AbsoluteTime[];
Print[StringTemplate["BlackBoxAMFlow: systems solved in `1`s."][ToStringInput[Ceiling[time2-time1]]]];
If[$DeleteBlackBoxDirectory===True, DeleteDir[$AMFDirectory]];
sol
];


(* ::Subsubsection::Closed:: *)
(*automatic computation*)


FitEps[epslist_, vlist_, leading_]:=Fit[Transpose[{epslist, vlist}], Power[ep, leading+Range[0, Length@epslist-1]], ep];


GenerateNumericalConfig[goal_, order_]:=Block[{number,eps0,epslist,singlepre,workpre,xorder},
number = Ceiling[5*order/2+2*Length[Loop]];
eps0 = Power[10, -Length[Loop]/2-goal/(order+1)];
eps0 = Rationalize[N[eps0], eps0/100];
If[number>100, Print["GenerateNumericalConfig: the given order is too large to evaluate."]; Abort[]];
epslist = eps0 + Range[number] * eps0/100;
singlepre = Max[Ceiling[(number+2*Length[Loop])*(Length[Loop]/2+goal/(order+1))], 30];
workpre = 2*singlepre;
xorder = 4*singlepre;
{epslist, workpre, xorder}
];


SolveIntegrals[jints_, goal_, order_]:=Block[{time1,masters,rules,epslist,$WorkingPre,$XOrder,sol,time2},
Print["SolveIntegrals: started."];
time1 = AbsoluteTime[];
{masters, rules} = BlackBoxReduce[jints, {}, CacheDirectory["solveint_reduce"]];
If[masters==={}, Return[{}]];

{epslist, $WorkingPre, $XOrder} = GenerateNumericalConfig[goal, order];
Print["SolveIntegrals: integrals will be evaluated at "<>ToStringInput[Length[epslist]]<>" eps values" -> epslist];
Print["SolveIntegrals: working precision and truncated order" -> {$WorkingPre, $XOrder}];

sol = BlackBoxAMFlow[masters, epslist, CacheDirectory["solveint_amflow"]];
If[!SubsetQ[Keys[sol], masters], Print["SolveIntegrals: some master integrals have not been solved yet."]; Abort[]];
sol = Transpose[masters/.sol];

sol = Block[{$MinPrecision = $WorkingPre, $MaxPrecision = $WorkingPre, leading, jintsvalue},
leading = -2*Length[Loop];
jintsvalue = Transpose@Table[(Values[rules]/.ep -> epslist[[i]]) . sol[[i]], {i, Length[epslist]}];
jintsvalue = FitEps[epslist, #, leading]&/@jintsvalue;
Chop[Series[#, {ep, 0, leading + order}]&/@jintsvalue//Normal, 10^-$ChopPre]];

time2 = AbsoluteTime[];
Print[StringTemplate["SolveIntegrals: finished in `1`s."][ToStringInput[Ceiling[time2-time1]]]];
Thread[Keys[rules] -> N[sol, goal]]
];


ExpandGaugeX[dir_]:=Module[{name,template,rule,time},
name = FileNameJoin[{dir, "solve"}];
template = StringToTemplate[{
"If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName//SetDirectory;
Get[`desolver`];
{masters, table} = Get[\"reduction\"];
{mastersde, variables, diffeq} = Get[\"diffeq\"];
{point, boundary} = Get[\"boundary\"];",
"sol[n_]:=Module[{tablecoe,de,bc,masterexp,allexp},
SetDefaultOptions[];
SetGlobalOptions[\"WorkingPre\" -> `wkpre`, \"ChopPre\" -> `cppre`];
SetExpansionOptions[\"XOrder\" -> `xorder`, \"LearnXOrder\" -> `lorder`, \"TestXOrder\" -> `torder`];
tablecoe = Values[table]/.{variables[[1]] -> eta, eps -> boundary[[n,1]]}//Factor;
de = diffeq[[1]]/.{variables[[1]] -> eta, eps -> boundary[[n,1]]}//Factor;
bc = boundary[[n,2]];
LoadSystem[n, de, bc, point[[2]]];
SolveAsyExp[n];
masterexp = masters/.Thread[mastersde -> AsyExp[n]];
allexp = PlusAsyExp@MapThread[TimesAsyExp, {#, masterexp}]&/@tablecoe;
PickZeroRuleS/@allexp];",
"LaunchKernels[`kernel`];
results = ParallelTable[sol[i], {i, Length[boundary]}, DistributedContexts -> All];
CloseKernels[];",
"Put[Thread[Keys[table] -> Transpose[results]], \"solution\"];",
"Quit[];"}];

rule = <|
"desolver" -> ToStringInput[$DESolverPath],
"wkpre" -> ToStringInput[$WorkingPre],
"cppre" -> ToStringInput[$ChopPre],
"xorder" -> ToStringInput[$XOrder],
"lorder" -> ToStringInput[$LearnXOrder],
"torder" -> ToStringInput[$TestXOrder],
"kernel" -> ToStringInput[NThread]
|>;

FileTemplateApply[template, rule, name<>".wl"];
Print["ExpandGaugeX: expanding integrals near 0."];
time = RunCommand[{$WolframPath, "-noprompt", "-script", name<>".wl"}, "log"->name<>".log"];
Print[StringTemplate["ExpandGaugeX: integrals expanded in `1`s."][ToStringInput[Ceiling[time]]]];
];


SolveIntegralsGaugeLink[jints_, goal_, order_]:=Block[{time1,time2,symbol,posi,dir,master,table,extra,masterde,vars,diffeq,epslist,$WorkingPre,$XOrder,min,sol,fit},
Print["SolveIntegralsGaugeLink: started."];
time1 = AbsoluteTime[];
symbol = Symbol[ToStringInput[Family]<>"x"];
SaveGlobalVariables[symbol];
AMFlowInfo["Propagator"] = GenerateSquare[#, symbol]&/@Propagator;
posi = If[!FreeQ[Propagator[[#]], symbol], #, Nothing[]]&/@Range[Length[Propagator]];
If[posi==={}, 
Print["SolveIntegralsGaugeLink: nothing to do with the propagators. Use SolveIntegrals instead."]; 
Return[SolveIntegrals[jints, goal, order]]];

dir = CacheDirectory["solveint_gauge_asyexp"];
CreateDir[dir];
Print["SolveIntegralsGaugeLink: temporary family created."];
PrintGlobalVariables[];

{master, table} = BlackBoxReduce[jints, {}, CacheDirectory["solveint_gauge_reduce"]];
extra[jint_]:=Power[symbol, -Total[(List@@jint)[[posi+1]]]];
Put[{master, Thread[Keys[table] -> extra/@Keys[table]*Values[table]]}, FileNameJoin[{dir, "reduction"}]];

{masterde, vars, diffeq} = BlackBoxDiffeq[master, {symbol}, CacheDirectory["solveint_gauge_diffeq"]];
Put[{masterde, vars, diffeq}, FileNameJoin[{dir, "diffeq"}]];
If[!SubsetQ[masterde, master], Print["SolveIntegralsGaugeLink: some master integrals are not included in the differential equations."]; Abort[]];

{epslist, $WorkingPre, $XOrder} = GenerateNumericalConfig[goal, order];
Print["SolveIntegralsGaugeLink: integrals will be evaluated at "<>ToStringInput[Length[epslist]]<>" eps values" -> epslist];
Print["SolveIntegralsGaugeLink: working precision and truncated order" -> {$WorkingPre, $XOrder}];

min = Block[{factors, zero, rationscale, polelist}, 
factors = FactorList[Times@@(diffeq/.ep -> epslist[[1]]//Denominator//Flatten)][[2;;,1]];
zero[poly_]:=If[#=!={}, symbol/.#, {}]&/@NSolve[poly==0, symbol, WorkingPrecision -> $WorkingPre];
rationscale = Power[10, -20];
polelist = Rationalize[Join@@(zero/@factors), rationscale];
Rationalize[Select[polelist, #=!=0&]//Abs, rationscale]//Min];

AMFlowInfo["Propagator"] = Propagator/.symbol -> min/10;
sol = BlackBoxAMFlow[masterde, epslist, CacheDirectory["solveint_gauge_amflow"]];
If[!SubsetQ[Keys[sol], masterde], Print["SolveIntegralsGaugeLink: some master integrals have not been solved yet."]; Abort[]];
sol = Thread[epslist -> Transpose[masterde/.sol]];
Put[{symbol -> min/10, sol}, FileNameJoin[{dir, "boundary"}]];
LoadGlobalVariables[symbol];

ExpandGaugeX[dir];
sol = GetFile[FileNameJoin[{dir, "solution"}]];
fit = Block[{$MinPrecision = $WorkingPre, $MaxPrecision = $WorkingPre, leading, jintsvalue},
leading = -2*Length[Loop];
jintsvalue = FitEps[epslist, #, leading]&/@Values[sol];
Chop[Series[#, {ep, 0, leading + order}]&/@jintsvalue//Normal, 10^-$ChopPre]];

time2 = AbsoluteTime[];
Print[StringTemplate["SolveIntegralsGaugeLink: finished in `1`s."][ToStringInput[Ceiling[time2-time1]]]];
If[$DeleteBlackBoxDirectory===True, DeleteDir[dir]];
Thread[Keys[sol] -> N[fit, goal]]
];


(* ::Subsection::Closed:: *)
(*end*)


InitializePackage[];


End[];


EndPackage[];
